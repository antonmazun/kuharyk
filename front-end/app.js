import $ from 'jquery';
import jQuery from 'jquery';
import slick from 'slick-carousel/slick/slick'
window.$ = jQuery;
var App = App || {};
window.App = App;

App.main  =(function() {

    var header,
        mobileMenuButton,
        mobileMenu,
        mobileLinksList,
        switchableHeading,
        current_index,
        timeOffset,
        length ,
        service_slider,
        reviews_slider ,
        blog_slider;

    var init = function () {
        header = $('.header');
        mobileMenuButton = $('.mobile-menu-trigger');
        mobileMenu = $('.mobile-menu');
        mobileLinksList = mobileMenu.find('.menu-item');
        switchableHeading = $('.animated-description .switchable');
        service_slider = $('#service_slider');
        reviews_slider = $('#reviews_slider');
        timeOffset = 2000;
        current_index  = 0 ;
        length = switchableHeading.length - 1;
        blog_slider  = $('#blog_slider');
        switchHeadings();
        navigation();
        $('.home-section').addClass('zoomed');
        $('.cut-deco').addClass('visible');

        services_slider();
        full_width_slider();
        reviews_slider_init();
        blog_slider_init();
    };

    var switchHeadings = function() {
        setTimeout(function () {
            switchableHeading.eq(current_index).removeClass('visible').addClass('top-invisible');
            switchableHeading.eq(current_index + 1).addClass('visible');
            delete_class(switchableHeading.eq(current_index));
            current_index++;
            if (current_index <= length) {
                switchHeadings();
            } else {
                current_index = 0;
                switchableHeading.eq(current_index).removeClass('top-invisible').addClass('visible');
                switchHeadings();
            }
        }, timeOffset)
    };

    function delete_class(elem){
        setTimeout(function(){
            $(elem).removeClass('top-invisible');
        } , timeOffset/2 );
    }

    var navigation = function () {
        var resolution = window.innerWidth;

        window.onresize = function () {
            resolution = window.innerWidth;
        };
        if(resolution > 1024) {
            if ( $(this).scrollTop() > header.height() ){
                header.addClass('sticky');
            }
            $(window).on('scroll', function () {
                if ( $(this).scrollTop() > header.height() ){
                    header.addClass('sticky');
                    return;
                }
                header.removeClass('sticky');
            })
        }

        mobileMenuButton.on('click', function () {
            if (mobileMenu.hasClass('open')){
                $(this).removeClass('active');
                mobileMenu.removeClass('open');
                return;
            }
            mobileMenu.addClass('open');
            $(this).addClass('active');
        });

        mobileLinksList.on('click', function () {
            if ( $(this).hasClass('open') ){
                mobileLinksList.removeClass('open').find('.dropdown-list').slideUp();
                return;
            }
            mobileLinksList.removeClass('open').find('.dropdown-list').slideUp();
            $(this).addClass('open').find('.dropdown-list').slideDown();
        });
    };


    var services_slider = function(){

        $(service_slider).slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    };

    var full_width_slider = function(){
        $('.js-slick').slick({
            autoplay: false,
            autoplaySpeed: 5000,
            dots: true,
            draggable: false,
            fade: false,
        });

        $('.js-slick').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
            $(slick.$slides).removeClass('is-animating');
        });

        $('.js-slick').on('afterChange', function(event, slick, currentSlide, nextSlide) {
            $(slick.$slides.get(currentSlide)).addClass('is-animating');
        });
    };

    var reviews_slider_init = function() {
        $(reviews_slider).slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    };

    var blog_slider_init = function() {
        $(blog_slider).slick({
            dots: true,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 568,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });
    };


    return{
        init: init
    }
})();
