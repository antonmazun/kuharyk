'use strict';
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');

module.exports = {
    mode: 'development',
    devtool: 'source-map',
    entry: {
        client: ['./front-end/app.js', './front-end/scss/style.scss'],
        vendors : ['./node_modules/jquery/dist/jquery.min.js'  , './node_modules/slick-carousel/slick/slick.min.js']
    },
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname, './dist')
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: 'vue-loader'
            },
            {
                test: /\.(sass|scss)$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: "css-loader"
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true
                        }
                    }
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader?name=./fonts/[name].[ext]'
                    }
                ]
            },
            {
                test: /\.(png|jpg)$/,
                loader: 'url-loader'
            }
        ]
    },
    plugins: [

        new MiniCssExtractPlugin({
            filename: "[name].min.css",
        })
    ]
};